$(function() {
    //Se inicializa el tooltip
    $("[data-toggle='tooltip']").tooltip();

    //Se inicializa el popover
    $("[data-toggle='popover']").popover();

    //Se le estabece un tiempo de intervalo entre slide del carousel
    $('.carousel').carousel({
        interval: 2000
    });

     //***Eventos del componente modal #info***//

     //Eventos show y shown
    $('#info').on('show.bs.modal', function(e) {
        console.log('El modal de más info se está mostrando.');

        //Se desactiva el botón del modal al comenzar a mostrarse el mismo
        $("[data-target='#info']").prop('disabled', true);
    });
    $('#info').on('shown.bs.modal', function(e) {
        console.log('El modal de más info se mostró.');
    });

    //Eventos hide y hidden
    $('#info').on('hide.bs.modal', function(e) {
        console.log('El modal de más info se está ocultando.');
    });
    $('#info').on('hidden.bs.modal', function(e) {
        console.log('El modal de más info se ocultó.');

        //Se vuelve a activar el botón del modal al ocultarse el mismo
        $("[data-target='#info']").prop('disabled', false);
    });

    //***Eventos del componente modal #registro***//

     //Eventos show y shown
     $('#registro').on('show.bs.modal', function(e) {
        console.log('El modal de registro se está mostrando.');

        //Se desactiva el botón del modal al comenzar a mostrarse el mismo
        $("#btn-registro").prop('disabled', true);
    });
    $('#registro').on('shown.bs.modal', function(e) {
        console.log('El modal de registro se mostró.');
    });

    //Eventos hide y hidden
    $('#registro').on('hide.bs.modal', function(e) {
        console.log('El modal de registro se está ocultando.');
    });
    $('#registro').on('hidden.bs.modal', function(e) {
        console.log('El modal de registro se ocultó.');

        //Se vuelve a activar el botón del modal al ocultarse el mismo
        $("#btn-registro").prop('disabled', false);
    });
});